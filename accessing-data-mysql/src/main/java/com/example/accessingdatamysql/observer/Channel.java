package com.example.accessingdatamysql.observer;

public interface Channel {
    public void update(String news);
}
