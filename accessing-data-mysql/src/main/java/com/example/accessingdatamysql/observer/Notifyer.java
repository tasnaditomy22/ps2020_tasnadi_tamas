package com.example.accessingdatamysql.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Observable with a list of observers.
 * Has a string(news) to be updated.
 * Any change on news notifies all observers.
 */
public class Notifyer {
    private String news;
    private List<Channel> channels = new ArrayList<>();

    /**
     * Adds an observer to the list of channels
     * @param channel the observer to be added
     */
    public void addObserver(Channel channel) {
        this.channels.add(channel);
    }

    /**
     * Removes an observer to from list of channels
     * @param channel the observer to be removed
     */
    public void removeObserver(Channel channel) {
        this.channels.remove(channel);
    }


    /**
     * Updates own news and notifies observers
     * @param news String to be updated
     */
    public void setNews(String news) {
        this.news = news;
        for (Channel channel : this.channels) {
            channel.update(this.news);
        }
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
