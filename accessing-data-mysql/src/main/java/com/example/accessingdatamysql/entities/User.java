package com.example.accessingdatamysql.entities;

import com.example.accessingdatamysql.observer.Channel;
import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "users")
public class User implements Channel{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private String password;

    private String lastNotification;

    private boolean commonOrderRight;

    public User() {
    }

    public User(String name, String password){
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastNotification() {
        return lastNotification;
    }

    public void setLastNotification(String lastNotification) {
        this.lastNotification = lastNotification;
    }

    public boolean hasCommonOrderRight() {
        return commonOrderRight;
    }

    public void setCommonOrderRight(boolean commonOrderRight) {
        this.commonOrderRight = commonOrderRight;
    }

    public boolean isCommonOrderRight() {
        return commonOrderRight;
    }

    @Override
    public void update(String news) {
        this.setLastNotification((String) news);
    }
}