package com.example.accessingdatamysql.entities.userTypes;

import com.example.accessingdatamysql.entities.User;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

    public Admin() {
    }

    public Admin(String name, String password){
        super(name, password);
        setCommonOrderRight(true);
    }
}
