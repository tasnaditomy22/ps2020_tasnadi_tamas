package com.example.accessingdatamysql.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Product> products = new ArrayList<Product>();

    @Column(nullable = false)
    private int totalPrice;

    @Column(nullable = false)
    private int userId;

    public Order(){

    }

    public Order(List<Product> products, int userId){
        this.userId = userId;
        this.products = products;
        for (Product p: products
             ) {
            this.totalPrice += p.getPrice();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
