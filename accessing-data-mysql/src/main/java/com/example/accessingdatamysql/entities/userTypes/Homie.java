package com.example.accessingdatamysql.entities.userTypes;

import com.example.accessingdatamysql.entities.User;

import javax.persistence.Entity;

@Entity
public class Homie extends User {

    public Homie() {
    }

    public Homie(String name, String password){
        super(name, password);
        setCommonOrderRight(false);
    }
}