package com.example.accessingdatamysql.repos;

import com.example.accessingdatamysql.entities.Debt;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DebtRepository extends CrudRepository<Debt, Integer> {
    Optional<Debt> findByUserName1AndUserName2(String userName1, String userName2);
    Iterable<Debt> findAllByUserName1(String userName1);
}