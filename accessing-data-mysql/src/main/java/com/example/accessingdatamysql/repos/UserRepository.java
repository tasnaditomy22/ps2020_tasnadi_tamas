package com.example.accessingdatamysql.repos;

import com.example.accessingdatamysql.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByNameIs(String name);
    Optional<User> findByNameAndPassword(String name, String password);
    Integer deleteByNameIs(String name);
}