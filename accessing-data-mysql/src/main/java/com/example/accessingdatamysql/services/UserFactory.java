package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.User;
import com.example.accessingdatamysql.entities.userTypes.Admin;
import com.example.accessingdatamysql.entities.userTypes.Homie;

/**
 * Factory design pattern for creating Admins and Homies as Users
 */
public class UserFactory {

    /**
     * @param name Name of the user to be created
     * @param password Password of the user to be created
     * @param type Integer representing the type of the user to be created: Admin(1) or Homie(0)
     * @return the created user
     */
    public User createUser(String name, String password, int type){
        User user = null;

        switch (type) {
            case 0:
                user = new Homie(name, password);
                break;
            case 1:
                user = new Admin(name, password);
                break;
            default:
                System.err.println("Unknown/unsupported user-type.");
        }

        return user;
    }
}
