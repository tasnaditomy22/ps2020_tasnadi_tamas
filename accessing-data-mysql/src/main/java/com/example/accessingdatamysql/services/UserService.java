package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.User;
import org.springframework.stereotype.Component;

@Component
public interface UserService {
    public User addNewUser(String name , String password, int type);

    public Iterable<User> getAllUsers();

    public User findUserByName(String name);

    public User findUserByNameAndPassword(String name, String password);

    public User updateUserPassword(String name, String password);

    public Integer deleteUserByName( String name);

    public void updateUserNotification();
}
