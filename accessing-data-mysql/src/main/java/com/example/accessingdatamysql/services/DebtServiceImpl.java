package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Debt;
import com.example.accessingdatamysql.repos.DebtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * DB operations for the Debt entity
 */
@Service
public class DebtServiceImpl implements DebtService{

    @Autowired
    private DebtRepository debtRepository;

    /**
     * Adds a new debt or updates the sum of an existing debt
     * @param userName1 Name of the user in debt
     * @param userName2 Name of the user who paid for the order
     * @param sum Total value of the order
     * @return the created Debt
     */
    @Transactional
    public Debt addNewDebt(String userName1, String userName2, int sum){
        Optional<Debt> debtResponse =  debtRepository.findByUserName1AndUserName2(userName1,userName2);
        Debt debt = null;
        if(debtResponse.isPresent()) {
            debt = debtResponse.get();
            debt.setSum(debt.getSum() + sum);
        } else {
            debt = new Debt(userName1,userName2,sum);
        }
        debtRepository.save(debt);
        return debt;
    }

    /**
     * Finds all debts
     * @return all Debts
     */
    @Transactional
    public Iterable<Debt> getAllDebts(){
        return debtRepository.findAll();
    }

    /**
     * Finds all debts for the user currently logged in
     * @param userName1 Name of the user
     * @return all debts of the user
     */
    @Transactional
    public Iterable<Debt> getAllDebtsForCurrentUser(String userName1){
        return debtRepository.findAllByUserName1(userName1);
    }

    /**
     * Deletes a debt
     * @param id Id of the Debt to be deleted
     */
    @Transactional
    public void deleteDebtById(Integer id){
        debtRepository.deleteById(id);
    }
}
