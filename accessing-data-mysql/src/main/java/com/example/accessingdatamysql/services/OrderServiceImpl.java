package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Order;
import com.example.accessingdatamysql.entities.Product;
import com.example.accessingdatamysql.repos.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


/**
 * DB operations for the Order entity
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;


    /**
     * Creates and saves a new order.
     * @param products a list of products to be added to the order
     * @param userId the Id of the user who placed the order
     * @return the saved order
     */
    @Transactional
    public Order addNewOrder( List<Product> products, int userId) {
        Order order = new Order(products, userId);
        orderRepository.save(order);
        return order;
    }

    /**
     * Finds all orders
     * @return the list of all the orders
     */
    @Transactional
    public Iterable<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    /**
     * Finds an order with given Id
     * @param id the Id of the order to look after
     * @return the order with the given Id
     */
    @Transactional
    public Order findOrderById(Integer id){
        Optional<Order> orderResponse =  orderRepository.findById(id);
        Order order = null;
        if(orderResponse.isPresent()) {
            order = orderResponse.get();
        }else {
            throw new RuntimeException("No record found for given order id: "+ id);
        }
        return order;
    }

    /**
     * Updates an order with given Id by adding one more product to it
     * @param orderId the Id of the order to be updated
     * @param productId the Id of the product to be added
     * @return the updated order
     */
    @Transactional
    public Order updateOrderByAddingProduct( Integer orderId, Integer productId){
        Product product = productService.findProductById(productId);
        Order order = this.findOrderById(orderId);
        order.getProducts().add(product);
        orderRepository.save(order);
        return order;
    }

    /**
     * Updates an order with given Id by removing a product from it
     * @param orderId the Id of the order to be updated
     * @param productId the Id of the product to be removed
     * @return the updated order
     */
    @Transactional
    public Order updateOrderByRemovingProduct( Integer orderId, Integer productId){
        Product product = productService.findProductById(productId);
        Order order = this.findOrderById(orderId);
        order.getProducts().remove(product);
        orderRepository.save(order);
        return order;
        // forgot to check if the product is in the list
    }

    /**
     * Deletes an order
     * @param id the Id of the order to be deleted
     */
    @Transactional
    public void deleteOrderById( Integer id){
        orderRepository.deleteById(id);
    }
}
