package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.User;
import com.example.accessingdatamysql.observer.Channel;
import com.example.accessingdatamysql.observer.Notifyer;
import com.example.accessingdatamysql.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * DB operations for the User entity
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    private UserFactory userFactory = new UserFactory();

    protected static Notifyer observable = new Notifyer();

    /**
     * When the application starts adds all existing products in the database to the list of observers
     */
    @PostConstruct
    public void init() {
        for (User u: userRepository.findAll()
             ) {
            observable.addObserver(u);
        }
    }

    /**
     * Creates and saves a new user
     * @param name the name of the new user
     * @param password the password of the new user
     * @return the saved user
     */
    @Transactional
    public User addNewUser( String name , String password, int type) {
        User user = userFactory.createUser(name,password,type);
        userRepository.save(user);
        observable.addObserver(user);
        return user;
    }

    /**
     * Finds all users
     * @return the list of all the users
     */
    @Transactional
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Finds a user with given name
     * @param name the name of the user to look after
     * @return the user with the given name
     */
    @Transactional
    public User findUserByName(String name){
        Optional<User> userResponse =  userRepository.findByNameIs(name);
        User user = null;
        if(userResponse.isPresent()) {
            user = userResponse.get();
        }else {
            throw new RuntimeException("No record found for given name: "+ name);
        }
        return user;
    }

    /**
     * Finds a user with given name and password
     * @param name the name of the user to look after
     * @param password the password of the user to look after
     * @return the user with the given name
     */
    @Transactional
    public User findUserByNameAndPassword(String name, String password){
        Optional<User> userResponse =  userRepository.findByNameAndPassword(name, password);
        User user = null;
        if(userResponse.isPresent()) {
            user = userResponse.get();
        }else {
            throw new RuntimeException("No record found for given name: "+ name);
        }
        return user;
    }

    /**
     * Updates the password of a given user
     * @param name the name of the user to be updated
     * @param password the new password of the user
     * @return the updated user
     */
    @Transactional
    public User updateUserPassword(String name, String password){
        User user = this.findUserByName(name);
        user.setPassword(password);
        userRepository.save(user);
        return user;
    }

    /**
     * Deletes a user
     * @param name the name of the user to be deleted
     * @return the number of deleted users
     */
    @Transactional
    public Integer deleteUserByName( String name){
        Integer response =  userRepository.deleteByNameIs(name);
        if(response == 0)
            throw new RuntimeException("No record found for given name: "+ name);
        return response;
    }

    /**
     * Updates the last notification of each user.
     * It gets triggered when a new product was added
     */
    @Transactional
    public void updateUserNotification(){
       List<Channel> channels = observable.getChannels();
       for (Channel c : channels){
           userRepository.save((User) c);
       }
    }

}
