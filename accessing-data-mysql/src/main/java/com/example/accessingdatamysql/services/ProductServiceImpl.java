package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Product;
import com.example.accessingdatamysql.repos.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * DB operations for the Product entity
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserService userService;

    /**
     * Creates and saves a new product.
     * Updates the news of the observable
     * Calls the method to update the notifications of users
     * @param name the name of the new product
     * @param quantity the quantity of the new product
     * @param price the price of the new product
     * @return the saved product
     */
    @Transactional
    public Product addNewProduct(String name, int quantity,  int price) {
        Product product = new Product(name, quantity, price);
        productRepository.save(product);
        UserServiceImpl.observable.setNews("New product added with name " + name);
        userService.updateUserNotification();
        return product;
    }

    /**
     * Finds all products
     * @return the list of all the products
     */
    @Transactional
    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    /**
     * Finds a product with given Id
     * @param id the Id of the product to look after
     * @return the product with the given Id
     */
    @Transactional
    public Product findProductById(Integer id){
        Optional<Product> productResponse =  productRepository.findById(id);
        Product product = null;
        if(productResponse.isPresent()) {
            product = productResponse.get();
        }else {
            throw new RuntimeException("No record found for given id: "+ id);
        }
        return product;
    }


    /**
     * Updates the quantity of a given product
     * @param id the Id of the product to be updated
     * @param quantity the new quantity of the product
     * @return the updated product
     */
    @Transactional
    public Product updateProductQuantity(Integer id,  int quantity){
        Product product = this.findProductById(id);
        product.setQuantity(quantity);
        productRepository.save(product);
        return product;
    }

    /**
     * Deletes a product
     * @param id the Id of the product to be deleted
     */
    @Transactional
    public void deleteProductById(Integer id){
        productRepository.deleteById(id);
    }
}
