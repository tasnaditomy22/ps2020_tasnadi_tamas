package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Debt;

public interface DebtService {

    public Debt addNewDebt(String userName1, String userName2, int sum);

    public Iterable<Debt> getAllDebts();

    public Iterable<Debt> getAllDebtsForCurrentUser(String userName1);

    public void deleteDebtById(Integer id);
}
