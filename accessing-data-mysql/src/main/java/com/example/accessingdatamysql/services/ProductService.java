package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Product;

public interface ProductService {

    public Product addNewProduct(String name, int quantity, int price);

    public Iterable<Product> getAllProducts();

    public Product findProductById(Integer id);

    public Product updateProductQuantity(Integer id,  int quantity);

    public void deleteProductById(Integer id);

}
