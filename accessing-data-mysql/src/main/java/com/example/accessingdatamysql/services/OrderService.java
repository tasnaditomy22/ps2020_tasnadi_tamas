package com.example.accessingdatamysql.services;

import com.example.accessingdatamysql.entities.Order;
import com.example.accessingdatamysql.entities.Product;

import java.util.List;

public interface OrderService {

    public Order addNewOrder(List<Product> products, int userId);

    public Iterable<Order> getAllOrders();

    public Order findOrderById(Integer id);

    public Order updateOrderByAddingProduct( Integer orderId, Integer productId);

    public Order updateOrderByRemovingProduct( Integer orderId, Integer productId);

    public void deleteOrderById( Integer id);
}
