package com.example.accessingdatamysql.controllers;

import com.example.accessingdatamysql.entities.Debt;
import com.example.accessingdatamysql.services.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@RequestMapping(path="/debt")
public class DebtController {

    @Autowired
    private DebtService debtService;

    @PostMapping(path="/add")
    public @ResponseBody
    Debt addNewDebt(@RequestParam String userName1, @RequestParam String userName2, @RequestParam int sum) {
        Debt debtResponse = (Debt) debtService.addNewDebt(userName1,userName2, sum );
        return debtResponse;
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Debt> getAllDebts() {
        return debtService.getAllDebts();
    }

    @GetMapping(path="/myDebts")
    public @ResponseBody Iterable<Debt> getAllDebtsForCurrentUser(String userName) {
        return debtService.getAllDebtsForCurrentUser(userName);
    }

    @Transactional
    @DeleteMapping(path="/delete")
    public void deleteDebt(@RequestParam Integer id){
        debtService.deleteDebtById(id);
    }
}
