package com.example.accessingdatamysql.controllers;

import com.example.accessingdatamysql.entities.Product;
import com.example.accessingdatamysql.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;


@Controller
@RequestMapping(path="/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping(path="/add")
    public @ResponseBody Product addNewProduct(@RequestParam String name, @RequestParam int quantity,
                                               @RequestParam int price) {
        return (Product) productService.addNewProduct(name,quantity,price);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @PutMapping(path="/update")
    public @ResponseBody Product updateProductQuantity(@RequestParam Integer id, @RequestParam int quantity){
        return (Product) productService.updateProductQuantity(id,quantity);
    }

    @Transactional
    @DeleteMapping(path="/delete")
    public void deleteProduct(@RequestParam Integer id){
        productService.deleteProductById(id);
    }

}