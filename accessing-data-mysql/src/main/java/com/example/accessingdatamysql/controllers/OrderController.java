package com.example.accessingdatamysql.controllers;

import com.example.accessingdatamysql.entities.Order;
import com.example.accessingdatamysql.entities.Product;
import com.example.accessingdatamysql.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@RequestMapping(path="/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(path="/add")
    public @ResponseBody Order addNewOrder(@RequestParam List<Product> products, @RequestParam int userId) {
        return (Order) orderService.addNewOrder(products, userId);
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @PutMapping(path="/updateOrder/addProduct")
    public @ResponseBody Order updateOrderByAddingProduct(@RequestParam Integer orderId, @RequestParam Integer productId){
        return (Order) orderService.updateOrderByAddingProduct(orderId, productId);
    }

    @PutMapping(path="/updateOrder/deleteProduct")
    public @ResponseBody Order updateOrderByRemovingProduct(@RequestParam Integer orderId, @RequestParam Integer productId){
        return (Order) orderService.updateOrderByRemovingProduct(orderId, productId);
    }

    @Transactional
    @DeleteMapping(path="/delete")
    public void deleteOrder(@RequestParam Integer id){
        orderService.deleteOrderById(id);
    }

}
