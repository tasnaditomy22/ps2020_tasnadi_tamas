package com.example.accessingdatamysql.controllers;

import com.example.accessingdatamysql.entities.User;
import com.example.accessingdatamysql.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Controller
@RequestMapping(path="/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(path="/add/homie")
    public @ResponseBody User addNewUser(@RequestParam String name, @RequestParam String password) {
        User userResponse = (User) userService.addNewUser(name,password,0);
        return userResponse;
    }

    @PostMapping(path="/add/admin")
    public @ResponseBody User addNewAdmin(@RequestParam String name, @RequestParam String password) {
        User userResponse = (User) userService.addNewUser(name,password,1);
        return userResponse;
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(path="/login")
    public @ResponseBody User getUser(@RequestParam String name, @RequestParam String password) {
        return userService.findUserByNameAndPassword(name, password);
    }

    @PutMapping(path="/update")
    public @ResponseBody User updateUserPassword(@RequestParam String name, @RequestParam String password){
        User userResponse = (User) userService.updateUserPassword(name,password);
        return  userResponse;
    }

    @Transactional
    @DeleteMapping(path="/delete")
    public @ResponseBody String deleteUser(@RequestParam String name){
        Integer response = userService.deleteUserByName(name);
        return response.intValue() + " user Deleted";
    }

}