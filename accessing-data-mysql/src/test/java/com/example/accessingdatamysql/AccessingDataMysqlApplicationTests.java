package com.example.accessingdatamysql;

import com.example.accessingdatamysql.entities.User;
import com.example.accessingdatamysql.observer.Notifyer;
import com.example.accessingdatamysql.services.UserFactory;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
class AccessingDataMysqlApplicationTests {

	@Test
	public void testObserverPattern(){
		Notifyer observable = new Notifyer();
		User observer = new User();

		observable.addObserver(observer);
		observable.setNews("doi");
		Assert.assertEquals("doi", observer.getLastNotification());
	}

	@Test
	public void testFactoryPatternCreateAdmin(){
		UserFactory userFactory = new UserFactory();
		User user = userFactory.createUser("user", "pwd", 1);
		Assert.assertEquals(true, user.hasCommonOrderRight());
	}

	@Test
	public void testFactoryPatternCreateHomie(){
		UserFactory userFactory = new UserFactory();
		User user = userFactory.createUser("user", "pwd", 0);
		Assert.assertEquals(false, user.hasCommonOrderRight());
	}
}